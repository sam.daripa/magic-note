// console.log("Magic Notes");
showNotes();//ShowNotes Function



//Event call to addnotes
let addBtn = document.getElementById('addBtn');
addBtn.addEventListener('click', (element) => {
    let addText = document.getElementById('addText');
    let addTitle = document.getElementById('addTitle');
    let webNotes = localStorage.getItem('webNotes');
    if (webNotes == null) {
        noteObj = {
            note:[],
            title:[],
            fav:[]
        };
    }
    else {
        noteObj = JSON.parse(webNotes);
    }
    noteObj.note.push(addText.value);
    noteObj.title.push(addTitle.value);
    noteObj.fav.push(0);
    localStorage.setItem('webNotes', JSON.stringify(noteObj));
    // localStorage.setItem('titles', JSON.stringify(titleObj));
    addText.value = '';
    addTitle.value = '';
    showNotes();
});

//Show Notes Function

function showNotes() {
    let webNotes = localStorage.getItem('webNotes');
    if (webNotes == null) {
        noteObj = {
            note:[],
            title:[],
            fav:[]
        };
    }
    else {
        noteObj = JSON.parse(webNotes);
    }
    let html = '';
    noteObj.note.forEach(function (element, index) {
        let ttl=noteObj.title[index];
        if(noteObj.fav[index]==0){
            html +=
             `<div  class=" noteCard card mx-2 my-2 bg-light" style="width: 15rem;">
                <div class="card-body">
                  <h5 class="card-title">${ttl}</h5>
                  <p class="card-text">${element}</p>
                  <button id='${index}'  onclick="deleteNote(this.id)" class="btn btn-primary my-2">Delete Note</button>
                  <button id='note${index}' onclick="setFav(this.id)" class="btn ${index} btn-success">Add Favourite</button>
                </div>
              </div>`
        }
        else{
            html +=
             `<div  class=" noteCard card mx-2 my-2 bg-light text-white bg-dark" style="width: 15rem;">
                <div class="card-body">
                  <h5 class="card-title">${ttl}</h5>
                  <p class="card-text">${element}</p>
                  <button id='${index}'  onclick="deleteNote(this.id)" class="btn btn-primary my-2">Delete Note</button>
                  <button id='note${index}' onclick="setFav(this.id)" class="btn btn-danger">Remove Favourite</button>
                </div>
                <div class="card-footer">
                  <small class="text-muted">The note here is in ❤️Favourite Mode. 
                  Due to this ,the Delete Button does not work.
                  </small>
                </div>
              </div>`
        }
    });
    let noteElement = document.getElementById('notes');
    if (noteObj.length != 0) {
        noteElement.innerHTML = html;
    } else {
        noteElement.innerHTML = `Nothing to show ! "Add a Notes" in note secton`;
    }
}


//Delete Notes Functon
function deleteNote(index) {
    // console.log(index);
    let webNotes = localStorage.getItem('webNotes');
    if (webNotes == null) {
        noteObj = {
            note:[],
            title:[],
            fav:[]
        };
    }
    else {
        noteObj = JSON.parse(webNotes);
    }
    if(noteObj.fav[index]==1)
    {
        showNotes();
    }
    else{
    noteObj.note.splice(index,1);
    noteObj.title.splice(index,1);
    noteObj.fav.splice(index,1);
    localStorage.setItem('webNotes', JSON.stringify(noteObj));
    showNotes();
    }

}


//Searching

let searchTxt= document.getElementById('searchText');
searchTxt.addEventListener('input',function(){
    // console.log("Searching : "+ searchTxt.value)
    let inpValue = searchTxt.value;
    let noteCard = document.getElementsByClassName('noteCard');
    Array.from(noteCard).forEach(function(element){
        let cardTxt = element.getElementsByTagName('p')[0].innerText;
        if(cardTxt.includes(inpValue)){
            element.style.display="block";
        }
        else{
            element.style.display="none"
        }
    })
});


// Set as Favourite
function setFav(index){
//   fav= document.getElementsByClassName(`${cls}`);
  let favCard= document.getElementById(`${index}`);
  let webNotes = localStorage.getItem('webNotes');
    if (webNotes == null) {
        noteObj = {
            note:[],
            title:[],
            fav:[]
        };
    }
    else {
        noteObj = JSON.parse(webNotes);
    }
  if( favCard.innerText=="Add Favourite")
  {
    favCard.innerText="Remove Fav";
    favCard.classList.add("btn-danger")
    favCard.classList.remove("btn-success");
    // console.log(typeof index,index.slice(4))
    noteObj.fav[Number(index.slice(4))]=1;
    // et addBtn= document.getElementById(`${index.slice(4)}`);
    // addBtn.style.visibility="hidden";
  }else{
    favCard.innerText="Add Fav";
    favCard.classList.add("btn-success");
    favCard.classList.remove("btn-danger");
    noteObj.fav[Number(index.slice(4))]=0;
  }
  localStorage.setItem('webNotes', JSON.stringify(noteObj));
   showNotes();
  console.log(favCard)

}